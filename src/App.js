import "./App.css";
import BooksPage from "./pages/booksPage/BooksPage.js";

function App() {
  return (
    <div>
      <header></header>
      <BooksPage />
    </div>
  );
}

export default App;
