import bookService from "../../services/book.service";
import React, { useEffect } from "react";
import { useNavigate } from 'react-router-dom';

export default function BookList() {
  const [books, setBooks] = React.useState([]);
  let navigate = useNavigate(); 

  useEffect(() => {
    console.log("retrieveBooks");
    bookService
      .getAll()
      .then((response) => {
        console.log(response.data);
        setBooks(response.data);
      })
      .catch((e) => {
        console.log(e);
      });
  }, []);

  function routeChange(path) {
    navigate(path);
  }

  return (
    <div className="w3-row-padding w3-padding-16 w3-center body" id="food">
      <div
        className="w3-quarter card"
        key={"add-book"}
        style={{
          cursor: "pointer",
          display: "flex",
          justifyContent: "center",
          alignItem: "center",
        }}
      >
        <b>
          <a href="./new-book">
            <h3>Add New Book</h3>
          </a>
        </b>
      </div>
      {books && books.length>0 ?
      (books.map((b, i) => (
        <div
          className="card"
          key={"book" + i}
          style={{
            cursor: "pointer",
          }}
          onClick={()=>{routeChange('/'+b.ID)}}
        >
          <h3>{b.Title}</h3>
          <p>{b.Description}</p>
          <p>
            <b>Stock: </b>
            {b.Stock}
          </p>
        </div>
      ))) : 
      <h3>No book found, please add new book into inventory.</h3>}
    </div>
  );
}
