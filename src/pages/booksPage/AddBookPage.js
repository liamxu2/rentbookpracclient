import { Component } from "react/cjs/react.production.min";
import bookService from "../../services/book.service";
import { TextField, TextareaAutosize, Button } from "@mui/material";

export default class AddBookPage extends Component {
  constructor(props) {
    super(props);
    this.onChangeTitle = this.onChangeTitle.bind(this);
    this.onChangeDescription = this.onChangeDescription.bind(this);
    this.onChangeStock = this.onChangeStock.bind(this);
    this.saveBook = this.saveBook.bind(this);
    this.addBook = this.addBook.bind(this);
    this.state = {
      newBook: [],
      submitted: false,
      title: "",
      description: "",
      stock: 0,
    };
  }

  onChangeTitle(e) {
    this.setState({
      title: e.target.value,
    });
  }
  onChangeDescription(e) {
    this.setState({
      description: e.target.value,
    });
  }
  onChangeStock(e) {
    this.setState({
      stock: Math.floor(e.target.value),
    });
  }

  saveBook() {
    var data = {
      title: this.state.title,
      description: this.state.description,
      stock: this.state.stock,
    };
    bookService
      .create(data)
      .then((response) => {
        this.setState({
          id: response.data.id,
          title: response.data.title,
          description: response.data.description,
          stock: response.data.published,
          status: response.data.status,
          submitted: true,
        });
        console.log(response.data);
      })
      .catch((e) => {
        console.log(e);
      });
  }

  addBook() {
    this.setState({
      id: null,
      title: "",
      description: "",
      stock: 0,
      submitted: false,
    });
  }

  render() {
    return (
      <div className="submit-form">
        {this.state.submitted ? (
          <div>
            <h4>
              <b>You submitted successfully!</b>
            </h4>
            <Button type="submit" variant="outlined" onClick={this.addBook}>
              Add Another Book
            </Button>
          </div>
        ) : (
          <div>
          <h3>Add New Book</h3>
            <div>
              <TextField
                className="input"
                id="title"
                name="title"
                label="Title"
                type="text"
                value={this.state.title}
                onChange={this.onChangeTitle}
              />
            </div>
            <br />
            <div>
              <label>
                <b>Description</b>
                <br />
              </label>
              <TextareaAutosize
                style={{ width: 300, height: 55 }}
                type="text"
                label="Description"
                id="description"
                required
                value={this.state.description}
                onChange={this.onChangeDescription}
                name="description"
              />
            </div>
            <br />
            <div>
              <TextField
                type="number"
                className="input"
                id="stock"
                name="stock"
                label="Stock"
                required
                value={this.state.stock}
                onChange={this.onChangeStock}
              />
            </div>
            <br />
            <Button
              variant="contained"
              onClick={this.saveBook}
              className="btn btn-success"
            >
              Submit
            </Button>
          </div>
        )}
      </div>
    );
  }
}
