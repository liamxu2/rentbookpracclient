import { Component } from "react/cjs/react.production.min";
import NavBar from "../../components/navBar.js";
import BookList from "./BookList.js";
import AddBookPage from "./AddBookPage";
import EditBookPage from "./EditBookPage";

import { BrowserRouter, Routes, Route } from "react-router-dom";

export default class BooksPage extends Component {
  constructor(props) {
    super(props);
    this.sideBarStateChange = this.sideBarStateChange.bind(this);
    this.state = { books: [], sideBarDisplay: false };
  }

  sideBarStateChange = (v) => {
    this.setState({ sideBarDisplay: v });
  };

  render() {
    return (
      <div>
        {/* <!-- Sidebar (hidden by default) --> */}
        {this.state.sideBarDisplay && <NavBar />}

        {/*  <!-- Top menu --> */}
        <div className="w3-top">
          <div
            className="w3-white w3-xlarge"
            style={{ maxWidth: 1200, margin: "auto" }}
          >
            <div
              className="w3-button w3-padding-16 w3-left"
              onClick={() => this.sideBarStateChange(true)}
            >
              &#9776;
            </div>
            <div className="w3-center w3-padding-16">My Books</div>
          </div>
        </div>

        {/* <!-- !PAGE CONTENT! --> */}
        <div
          className="w3-main w3-content w3-padding"
          style={{
            maxWidth: 1200,
            marginTop: 100,
            display: "flex",
            justifyContent: "center",
          }}
        >
          <BrowserRouter>
            <Routes>
              <Route path="/" exact element={<BookList />} />
              <Route path="/new-book" exact element={<AddBookPage />} />
              <Route path="/:id" exact element={<EditBookPage />} />
            </Routes>
          </BrowserRouter>

          <hr />

          {/* <!-- Footer --> */}
          <footer className="w3-row-padding w3-padding-32"></footer>
        </div>
      </div>
    );
  }
}
