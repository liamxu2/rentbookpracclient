// import { Component } from "react/cjs/react.production.min";
import { useState, useEffect } from "react";
import bookService from "../../services/book.service";
import { TextField, TextareaAutosize, Button } from "@mui/material";

import { useParams } from "react-router-dom";

export default function EditBookPage() {
  let id = Math.floor(parseInt(useParams().id));

  let [title, setTitle] = useState("");
  let [description, setDescription] = useState("");
  let [stock, setStock] = useState(0);
  let [validBook, setIfBookValid] = useState(false);
  let [messageDisplay, setMessageDisplay] = useState("");

  const getBook = (id) => {
    bookService
      .get(id)
      .then((response) => {
        console.log(response.data);
        if (response.data && response.data.ID > -1) {
          setDescription(response.data.Description);
          setTitle(response.data.Title);
          setStock(response.data.Stock);
          setIfBookValid(true);
        } else {
          setIfBookValid(false);
          setMessageDisplay(
            "Cannot retrive book #" +
              id +
              ": " +
              JSON.stringify(response.data) +
              "   Click link to go back to book list."
          );
        }
      })
      .catch((e) => {
        console.log(e);
      });
  };
  const updateBook = () => {
    const book = {
      ID: Math.floor(Math.floor(parseInt(id))),
      Stock: stock,
      Description: description,
      Title: title,
    };
    console.log("updating book", book);
    bookService
      .update(book)
      .then((response) => {
        // console.log(response.data);
        let dt = response.data;
        if (dt.ID) {
          setDescription(dt.Description);
          setTitle(dt.Title);
          setStock(dt.Stock);
          setIfBookValid(true);
          setMessageDisplay("Book Updated! Click to go back to book list");
        } else {
          setMessageDisplay(
            "Book update failed: " +
              JSON.stringify(response.data) +
              ". Please try update book later. Click to go back to book list"
          );
        }
      })
      .catch((e) => {
        setMessageDisplay(
          "Book update failed: " +
            JSON.stringify(e) +
            ". Please try update book later. Click to go back to book list"
        );
      });
  };
  const deleteBook = () => {
    bookService
      .delete(Math.floor(parseInt(id)))
      .then((response) => {
        // console.log(response.data);
        let dt = response.data;
        if (dt.includes("removed")) {
          setIfBookValid(false);
          setMessageDisplay("Book removed! Click to go back to book list");
        } else {
          setIfBookValid(false);
          setMessageDisplay(
            "Book delete failed: " +
              JSON.stringify(dt) +
              ". Please try delete book later. Click to go back to book list"
          );
        }
      })
      .catch((e) => {
        setIfBookValid(false);
        setMessageDisplay(
          "Book delete failed: " +
            JSON.stringify(e) +
            ". Please try delete book later. Click to go back to book list"
        );
      });
  };

  useEffect(() => {
    getBook(id);
  }, [id]);

  return (
    <div>
      {validBook ? (
        <div>
          <h3>Update Book #{id}</h3>
          <br />
          <form>
            <div>
              <TextField
                className="input"
                id="title"
                name="title"
                label="Title"
                type="text"
                value={title}
                onChange={(ev) => setTitle(ev.target.value)}
              />
            </div>
            <br />
            <div>
              <label>
                <b>Description</b>
                <br />
              </label>
              <TextareaAutosize
                style={{ width: 300, height: 55 }}
                type="text"
                id="description"
                label="Description"
                name="description"
                onChange={(ev) => {
                  console.log(ev.target.value);
                  setDescription(ev.target.value);
                }}
                value={description}
              />
            </div>
            <br />
            <div>
              <TextField
                type="number"
                className="input"
                id="stock"
                name="stock"
                label="Stock"
                value={stock}
                onChange={(ev) => setStock(Math.floor(ev.target.value))}
              />
            </div>
            <br />
          </form>
          <Button
            variant="contained"
            style={{ marginRight: 20 }}
            color="error"
            onClick={() => deleteBook()}
          >
            Delete
          </Button>
          <Button type="submit" 
              variant="contained" onClick={() => updateBook()}>
            Update
          </Button>
        </div>
      ) : (
        <div>
          <br />
          {messageDisplay && messageDisplay.length === 0 && (
            <p>Please select a valid book...</p>
          )}
        </div>
      )}

      {messageDisplay && messageDisplay.length > 0 && (
        <b>
          <br />
          <a href="/">{messageDisplay}</a>
        </b>
      )}
    </div>
  );
}
