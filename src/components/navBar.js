import React from "react";
import { Component } from "react/cjs/react.production.min";

export default class NavBar extends Component {
  constructor(props) {
    super(props);
    console.log(props);
  }
  
  render() {
    return (
      <div>
        <nav
          className="w3-sidebar w3-bar-block w3-card w3-top w3-xlarge w3-animate-left"
          style={{ zIndex: 2, width: "40%", minWidth: 300, display: "block" }}
          id="mySidebar"
        >
          {/* <a
            href="."
            className="w3-bar-item w3-button"
          >
            Close Menu
          </a> */}
          <a href="./" className="w3-bar-item w3-button">
            Books
          </a>
          <a href="./new-book" className="w3-bar-item w3-button">
            Add New Book
          </a>
        </nav>
      </div>
    );
  }
}
