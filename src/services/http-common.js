import axios from "axios";

export default axios.create({
  baseURL: "http://localhost:8443/",
  headers: {
    Accept: "*/*",
    "Content-type": "application/json"
  },
});
