import http from "./http-common.js";

class BookDataService {
  getAll() {
    return http.get("/");
  }
  get(id) {
    return http.get(`/${id}`, id);
  }
  create(data) {
    return http.post("/", data);
  }
  update(data) {
    return http.put("/", data);
  }
  delete(id) {
    return http.delete(`/${id}`, id);
  }
}
export default new BookDataService();
